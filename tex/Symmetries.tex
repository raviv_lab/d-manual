
\documentclass[../D+Manual.tex]{subfiles}
\begin{document}

\chapter{Symmetries} \label{chp:Symmetries}

\begin{quote}
	
	
	The universe is built on a plan the profound symmetry of which is somehow present in the inner structure of our intellect.\\
	\hspace*{\fill} \textit{Paul Valery}
\end{quote}

\section{Introduction}

Supramolecular complex structures can be often described as hierarchical trees in which repeating subunits are docked into their assembly symmetries. The assembly symmetries describe how repeating subunits are organized is space, that is their locations and orientations (see Figure \ref{fig:hierarchical}). For large assemblies, the scattering amplitude is given by: 

\begin{equation}
\label{eqn:assemblyScatteringAmplitude}
F\left(\vec{q}\right)=
\sum_{j=1}^{J}
\sum_{m=1}^{M_j}
\left[F_{j}\left(\mathbf{A}_{j,m}^{-1}\vec{q}\right)\cdot
\exp\left(i\vec{q}\cdot\vec{R}_{j,m}\right)\right]
\end{equation}
where $F\left(\vec{q}\right)$ is the scattering amplitude from an assembly comprised of $n_s$ subunits. $J$ is the number of different types of objects, which is also the total number of leaves in the hierarchical tree structure representation of the entire supramolecular assembly (Figure \ref{fig:hierarchical}). The leaves can either be geometry-based or atomic-based models, taken, for example, from Protein Data Bank (PDB) files. 
$M_j$ is the number of instances of object type $j$, whose orientations and positions are determined by rotation matrices  $\mathbf{A}_{j,m}$ and real-space translation vectors $\vec{R}_{j,m}$, respectively. The total number of subunits, $n_s$, is therefore  $\sum_{j=1}^{J}{M_j}$.

To evaluate the scattering of a complex structure, we need a way to describe ``Assembly Symmetries''. There are three methods to do so within D+.



\begin{figure}[h]
	\centering
%	\tikzsetnextfilename{Tree}	% name next TikZ figure
	\input{Tree.tex}	% input TiKZ figure code - See more at: http://www.howtotex.com/tips-tricks/faster-latex-part-ii-external-tikz-library/#sthash.daLZMQjk.dpuf
%	\includegraphics[width=\linewidth]{Tree}
	\caption {Example of modeling a supramolecular assembly in a hierarchical manner. Each \textit{Assembly Symmetry}  may contain multiple children. Children can either be additional \textit{Assembly Symmetries} or \textit{Subunits}. A \textit{Subunit} represents an atomic model (for example, a Protein Data Bank (PDB) file) or a geometric model. Internal nodes consist of \textit{Assembly Symmetries}, whereas each leaf must be a \textit{Subunit}.  There may be an arbitrary number of hierarchy levels and nodes in each level. Many supramolecular structures may be constructed that way. Care, however, should be taken when dealing with very large structures (see \hyperref[sec:hybrid]{\textbf{Hybrid Calculations}} for details).}
	\label{fig:hierarchical}
\end{figure}


\section{Space-Filling Symmetry}
\subsection{Background}
\begin{figure} %[6]{r}{0.5\textwidth}
	\centering
	\tikzsetnextfilename{BasisVector}	% name next TikZ figure
	\input{BasisVector.tex}	% input TiKZ figure code - See more at: http://www.howtotex.com/tips-tricks/faster-latex-part-ii-external-tikz-library/#sthash.daLZMQjk.dpuf
	\caption{Unit cell vectors. $a^j$ is the length of $\vec{A}^{,j}_1$, which is laying along the $x$-axis, $b^j$ is the length of $\vec{A}^{,j}_2$, which is within the $xy$ plane, and $c^j$ is the length of $\vec{A}^{,j}_3$. The angles between the unit cell vectors are indicated in the Figure.}
	\label{fig:unitcellvectors}
\end{figure}

In this method, all child subunits are treated as a unit cell of a primitive Bravais lattice. Copies of unit cell object type $j$ at the same orientation, given by  matrix $\mathbf{A}_{j}$, are placed at $R_{j,m}=R^j_{n^j_1,n^j_2,n^j_3}=n^j_1\cdot\vec{A}^j_1+n^j_2\cdot\vec{A}^j_2+n^j_3\cdot\vec{A}^j_3$, where $m$ is an index that  corresponds to $\left\lbrace n^j_1,n^j_2,n^j_3\right\rbrace$, which is any combination of three integers between $\left\lbrace 0,0,0\right\rbrace$ and  $\left\lbrace\left( N_1^{j}-1\right),\left(N_2^{j}-1\right),\left(N_3^{j}-1\right)\right\rbrace$.
The unit cell vectors, $\vec{A}^j_1,\vec{A}^j_2$, and $\vec{A}^j_3$ are defined by three lengths $a^j,b^j$, and $c^j$ and three angles $\alpha^j,\beta^j$, and $\gamma^j$ (Figure \ref{fig:unitcellvectors}).
The total number of subunit copies, $M_j$, of object $j$ will be $N_1^{j}\times N_2^{j}\times N_3^{j}$. The contribution of the space-filling lattice, comprise of repeating subunits of type $j$, to the scattering amplitude, $F_J$, is computed according to:
\begin{equation}
\label{eqn:assemblyScatteringAmplitudeSFS}
F_J\left(\vec{q}\right)=
F_{j}\left(\mathbf{A}_{j}^{-1}\vec{q}\right)\cdot\exp\left(i\vec{q}\cdot\vec{T}_{j}\right)\cdot SF^j(\vec{q}),
\end{equation}
where $\mathbf{A}_{j}$ is the rotation matrix by which subunit $j$ was rotated, $\vec{T}_j$ is a translation vector of the  entire space-filling lattice, and $SF^j(\vec{q})$ is the space-filling structure-factor, given by:
\begin{equation}
\label{eqn:assemblyScatteringSFS}
SF^j(\vec{q})=
\overset{\left(N_{1}^{j}-1\right)}{\underset{n^{j}_{1}=0}{\sum}}\overset{\left(N_{2}^{j}-1\right)}{\underset{n^{j}_{2}=0}{\sum}}\overset{\left(N_{3}^{j}-1\right)}{\underset{n^{j}_{3}=0}{\sum}}\exp\left(i\vec{q}\cdot\vec{R}^j_{n^{j}_{1},n^{j}_{2},n^{j}_{3}}\right).
\end{equation} 

The three unit vectors in real-space are constructed in the following way. We start with vector $\vec{A}^{j}_{1}$ of
length $a^j$ along the $x$ direction and vector $\vec{A}^{j}_{2}$ of length
$b^j$. The angle between the two vectors is $\gamma^j$. We then
add a third vector $\vec{A}^{j}_{3}$ defined by its length $c^j$
and two more angles $\alpha^j$ and $\beta^j$, where $\alpha^j$ is between
the vectors $\vec{A}^{j}_{1}$ and $\vec{A}^{j}_{3}$, and $\beta^j$ is between
the vectors $\vec{A}^{j}_{2}$ and $\vec{A}^{j}_{3}$. In real-space, the basis
vectors are given by:

\begin{equation*}
\vec{A}^{j}_{1}=(a^j,0,0),
\end{equation*}


\begin{equation*}
\vec{A}^{j}_{2}=\left(b^j\cos\gamma^j,b^j\sin\gamma^j,0\right),
\end{equation*}


\begin{equation*}
\vec{A}^{j}_{3}=\left(c^j\cos\alpha^j,\frac{c^j\cdot t^j}{\sin\gamma^j},\frac{c^j\cdot v^j}{\sin\gamma^j}\right),
\end{equation*}
where $t^j=\cos\beta^j-\cos\alpha^j\cos\gamma^j$ and  $v^j=\sqrt{\sin^{2}\gamma^j-\sin^{2}\gamma^j\cos^{2}\alpha^j-\left(t^j\right)^{2}}$. In real-space, the unit cell vectors $\vec{A}_{h}$ can then be rotated by a rotation matrix $\mathbf{O}_j$, so that the final unit cell vectors are: $\vec{a}_{h}^j=\mathbf{O}_j\vec{A}_{h}^{j}$, where $h\in\left\{ 1,2,3\right\}$, and then $R_{j,m}=R^j_{n^j_1,n^j_2,n^j_3}=n^j_1\cdot\vec{a}^j_1+n^j_2\cdot\vec{a}^j_2+n^j_3\cdot\vec{a}^j_3$. 



\subsubsection{What can we expect to find in reciprocal-space?}

 As the basis vectors in reciprocal space are given by:
\begin{equation*}
\vec{a}_{1}^{*j}=\frac{2\pi}{\upsilon ^j}\cdot\left(\vec{a}^j_2\times\vec{a}^j_3\right),\,  \vec{a}_{2}^{*j}=\frac{2\pi}{\upsilon ^j}\cdot\left(\vec{a}^j_3\times\vec{a}^j_1\right),\, \vec{a}_{3}^{*j}=\frac{2\pi}{\upsilon ^j}\cdot\left(\vec{a}^j_1\times\vec{a}^j_2\right) 
\end{equation*}
where $\upsilon ^j=\vec{a}^j_1\cdot\left(\vec{a}^j_2\times\vec{a}^j_3\right)$, we get:


\begin{equation*}
\vec{a}_{1}^{*j}=\left(\frac{2\pi}{a^j},\frac{-2\pi\cos\gamma^j}{a^j\sin\gamma^j},\frac{2\pi\left(t^j\cdot\cos\gamma^j-\cos\alpha^j\sin^{2}\gamma^j\right)}{a^j\cdot v^j\sin\gamma^j}\right),
\end{equation*}


\begin{equation*}
\vec{a}_{2}^{*j}=\left(0,\frac{2\pi}{b\sin\gamma^j},\frac{-2\pi t^j}{b^j\cdot v^j\sin\gamma^j}\right),
\end{equation*}


\begin{equation*}
\vec{a}_{3}^{*j}=\left(0,0,\frac{2\pi\sin\gamma^j}{c^j\cdot v^j}\right).
\end{equation*}

Any $\vec{q}$-vector, which is a linear combination of the reciprocal-space basis-vector, and is given by
\begin{equation*}
\vec{G}^j_{h_j,k_j,l_j}=h_j\vec{a}_{1}^{*j}+k_j\vec{a}_{2}^{*j}+l_j\vec{a}_{3}^{*j},
\end{equation*}
where $h_j,k_j$, and $l_j$ are integers, satisfies Bragg's condition, hence contributes (according to equation \ref{eqn:assemblyScatteringSFS}) to a structure-factor correlation peak at $\vec{q}=\vec{G}^j_{h_j,k_j,l_j}$. In solution, the contribution of $SF^j$ to the scattering curve will be at the scattering-vector amplitudes, $q$, which satisfy Bragg's condition, and are given by:

\begin{equation*}
q=\left|\vec{G}^j_{h_j,k_j,l_j}\right|=\left|h_j\vec{a}_{1}^{*j}+k_j\vec{a}_{2}^{*j}+l_j\vec{a}_{3}^{*j}\right|=
\end{equation*}
\begin{equation*}
\frac{2\pi}{a^j\cdot b^j\sin\gamma^j}\cdot
\sqrt{\begin{array}{c}
\left(h_jb^j\sin\gamma^j\right)^{2}+\left(k^j\cdot a^j-h_j\cdot b^j\cos\gamma^j\right)^{2}+\\
\frac{\left(h_jb^jc^j\left(\cos\gamma^j\cos\beta^j-\cos\alpha^j\right)-k_ja^jc^j\left(\cos\beta^j-\cos\alpha^j\cos\gamma^j\right)+l_ja^jb^j\sin^{2}\gamma^j\right)^{2}}{\left(c^j\right)^{2}\left(\sin^{2}\gamma^j-\cos^{2}\alpha^j-\cos^{2}\beta^j+2\cos\beta^j\cos\alpha^j\cos\gamma^j\right)}
\end{array}}.
\end{equation*}

\noindent The line-shape of the peaks will follow Equation \ref{eqn:assemblyScatteringAmplitudeSFS}.

Space-filling is a relatively easy way to create a regular Bravais lattice. For example, a 3D hexagonal lattice is defined by:

\begin{equation*}
\left\lbrace a^j = b^j ,c^j, \alpha^j = \beta^j = 90\degree, \gamma^j = 120\degree \right\rbrace
\end{equation*}

\noindent Figure \ref*{fig:3Dhexagonal} shows an example of a hexagonal lattice with five repeats in each direction.

\begin{figure} %[6]{r}{0.4\textwidth}

	%\vspace{-10pt}
	\centering
	%\fbox{
	%l b r t
    \includegraphics[trim={115pt 180pt 15pt 230pt},clip,width=0.45\linewidth]{"Hexagonal Lattice"}
    %}
	%\vspace{-20pt}
	\caption{Example of a 3D hexagonal lattice.}
	\label{fig:3Dhexagonal}
\end{figure}

%\subsection{Using Space-Filling Symmetry in D+}

\section{Manual Symmetry}
\label{sec:ManualSymmetry}

\begin{wrapfigure}{r}{0.6\textwidth}
	\vspace{-15pt}
	\centering
    \includegraphics[width=0.95\linewidth]{ParameterEditorManualSymmetry}
\end{wrapfigure}

Any three dimensional object in 3D space can be positioned and rotated by a set of six parameters: $\left\lbrace x,y,z\right\rbrace$ to determine its displacement and $\left\lbrace\alpha, \beta, \gamma\right\rbrace$ as Euler angles for rotation. A ``Manual Symmetry'' is a table of these six parameters per object. In the figure shown, there are two copies of the children positioned at two arbitrary positions with an arbitrary orientation. The list of coordinates and orientations can be loaded from a ``Docking List'' (DOL) text file (\texttt{*.dol}) only when adding a manual symmetry. The file should be tab or space delimited with each row starting with an index, followed by $\left\lbrace x,y,z\right\rbrace$ and finishing with $\left\lbrace \alpha, \beta, \gamma\right\rbrace$. A file that would create the symmetry in the image would look like:

\begin{lstlisting}[basicstyle=\small,breaklines=false,backgroundcolor=\color{gray!15}]
1	1	2	3	30	60	90
2	6	5	4	120	180	270
\end{lstlisting}

Note: The indices at the beginning of the line have no meaning, they just need to be integers.

\section{Scripted Symmetry}
\label{sec:ScriptedSymmetry}
\lstset{style=Luastyle,}

A scripted symmetry is a \href{http://www.lua.org/}{Lua} script that creates a \hyperref [sec:ManualSymmetry] {Manual Symmetry} behind the scenes based on parameters provided by the user.
The \href{http://www.lua.org/}{Lua} script has to contain an \lstinline|Information| table and several functions that will be presented below.
We shall run through an example script to show the usage.

\subsection{\lstinline|Information| Table}
\begin{sloppypar}
The \lstinline|Information| table has five fields: \lstinline|Name|; \lstinline|Type|; number of layer parameters \lstinline|(NLP)| ; \lstinline|MinLayers|; \lstinline|MaxLayers|. 
\end{sloppypar}

\begin{lstlisting}[basicstyle = \small]
Information = {
    Name = "Super Helical Right Hand Helix",  -- This is the name that will be displayed in the Domain View
    Type = "Symmetry", -- This is the type, should be "Symmetry" for scripted symmetries
    NLP = 1, -- Number of Layer Parameters: The number of parameters per layer
    MinLayers = 7, -- The minimal number of layers (<= MaxLayers)
    MaxLayers = 7, -- The maximal number of layers (>= MinLayers)
};
\end{lstlisting}

\subsection{\lstinline|Populate| Function}
Next, several functions need to be added. The first function to implement is the workhouse of the script. It populates the \hyperref [sec:ManualSymmetry] {Manual Symmetry}.
\begin{lstlisting}[basicstyle = \small,literate={'}{{'}}1,escapeinside={(*}{*)}]
function Populate(p, nlayers)
    -- This is really just a sanity check, but doesn't hurt to add it.	
	if (p == nil or nlayers (*$\sim$*)= 7 or table.getn(p[1]) (*$\sim$*)= 1) then				
		error("Parameter matrix must be 7x1, it is " .. nlayers .. "x" .. table.getn(p[1]));
	end
	
	-- Create meaningful names
	radius				= p[1][1];
	pitch				= p[2][1];
	unitsPerPitch		= p[3][1];
	unitsInPitch		= p[4][1];
	discreteHeight		= p[5][1];
	numHelixStarts		= p[6][1];
	superHelicalPitch	= p[7][1];
	
	-- Perform a few initial calcualtions	
	longitudinalSpacing = (pitch * 2.0 / numHelixStarts);
	
	angle		= 2.0 * math.pi / unitsPerPitch;
	if(superHelicalPitch >  0.00001) then
		-- angleShift is the amount in radians per dimer by which single longitudinal layer misses the 2(*{\color{blue!30!black!50!green}$\pi$}*) mark
		angleShift = (2.0 * math.pi * longitudinalSpacing) / (superHelicalPitch * unitsPerPitch);
	else
		angleShift = 0.0;
	end
	
	-- Create the table to return, and the indices
	res = {};
	n = 1;
	m = 1;
	
	ind = 0;

	-- Fill the res table
	for heightInd = 0, discreteHeight-1 do

		initialLayerShift = math.fmod(heightInd * (2.0 / numHelixStarts) * angleShift * unitsPerPitch, 2. * math.pi);
		
		hUnitsPerPitch = 2.0 * math.pi / (angle + angleShift * unitsPerPitch);

		initialZShift = heightInd * longitudinalSpacing;
		
		for inPitchInd = 0, unitsInPitch - 1 do
			theta = initialLayerShift + inPitchInd * (angle + angleShift);
			x		= radius * math.cos(theta);
			y		= radius * math.sin(theta);
			z		= initialZShift + (inPitchInd / unitsPerPitch) * pitch;
			alpha	= 0;
			beta	= 0;
			gamma	= 180. * theta / math.pi;

			--print(heightInd .. " " .. inPitchInd .. ": [" .. x .. ", " .. y ..  ", " .. z .. "]");
			
			res[ind+1] = {x,y,z,alpha,beta,gamma};
			ind = ind + 1;
		end
	end

	-- Return the complete table of x,y,z,\alpha,\beta,\gamma
	return res;

end
\end{lstlisting}


\subsection{\lstinline|GetLayerName| Function}
There are a few additional functions that are required for the user interface (UI). It needs to know things about the variables such as layer and parameter names, is it applicable, and default values. For starters, the text displayed to the left of the table in Figure \ref{fig:ParameterEditor} (section \ref{sec:parameterEditor}) is generated with the function below. Each row (or layer) gets the text based on its index, as shown.

\begin{lstlisting}[basicstyle = \small]
function GetLayerName(index)
	if index == 0 then
		return "Radius";
	elseif index == 1 then
		return "Pitch";
	elseif index == 2 then
		return "Units per Pitch";
	elseif index == 3 then
		return "Units in Pitch";
	elseif index == 4 then
		return "Discrete Height";
	elseif index == 5 then
		return "# Helix Starts";
	elseif index == 6 then
		return "Super Helical Pitch";
	else	
		return "N/A";
	end
end
\end{lstlisting}

\subsection{\lstinline|GetLayerParameterName| Function}
Next, the text above the table in Figure \ref{fig:ParameterEditor} is obtained with the function below. Each column (or layer parameter) gets the text based on its index, as shown.
\begin{lstlisting}[basicstyle = \small]
function GetLayerParameterName(index)
	if index == 0 then
		return "Parameter";
	else
		return "N/A"
	end
end
\end{lstlisting}

\subsection{\lstinline|IsParamApplicable| Function}
The applicability of a parameter is less obvious in this example. Here, all the parameters are applicable, so the function is a straightforward \lstinline|return true;|.

\begin{lstlisting}[basicstyle = \small]
function IsParamApplicable(layer, layerParam)
	return true;
end
\end{lstlisting}

\begin{wrapfigure}{r}{0.3\textwidth}
	\vspace{-10pt}
	\centering
    \includegraphics[trim={7pt 210pt 269pt 25pt},clip,width=0.95\linewidth]{ParameterEditorSlabs}
\end{wrapfigure}

A better (theoretical) example would be as shown in the image here. In this example, the ``Solvent'' layer does not have a ``Width.'' So the function should return true for all options \textit{except} when both \lstinline|layer| and \lstinline|layerParam| equal zero.

\begin{lstlisting}[basicstyle = \small]
function IsParamApplicable(layer, layerParam)
	if (layer == 0 and layerParam == 0) then
		return false;
	return true;
end
\end{lstlisting}

\subsection{\lstinline|GetDefaultValue| Function}
Finally, the UI needs to have some default values for each of the \lstinline|layer| \lstinline|layerParam| combinations. In the example case, the only dependency is  \lstinline|layer| and looks as follows.

\begin{lstlisting}[basicstyle = \small]
function GetDefaultValue(layer, layerParam)
	if layer == 0 then
		return 11.3;
	elseif layer == 1 then
		return 12.1;
	elseif layer == 2 then
		return 13.0;
	elseif layer == 3 then
		return 13.0;
	elseif layer == 4 then
		return 8;
	elseif layer == 5 then
		return 3;
	elseif layer == 6 then
		return 0.0;
	end
end
\end{lstlisting}




\end{document}


