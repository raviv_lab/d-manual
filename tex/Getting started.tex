
\documentclass[../D+Manual.tex]{subfiles}

%\externaldocument[P-]{Preface}
\begin{document}

\chapter{Getting started}
\label{sec:gettingstarted}

\begin{quote}
	The secret of getting ahead is getting started.\\
	\hspace*{\fill} \textit{Mark Twain (or not)}
\end{quote}

D+ computes the X-ray scattering from large supramolecular structures in solutions with high resolution.
Both geometric and atomic models can be computed and combined in hierarchical manner to form complex structures.
D+ can be launched either in remote mode or in local mode.
By default, D+ launches in local mode.
To launch D+ in the remote mode, run \path{DPlus.exe --remote}.
This can be simplified by creating a shortcut to \path{DPlus.exe} and adding \path{--remote} to the target or by creating a batch script doing the same.
If installed with the installer, there will be a \path{D+ Remote} shortcut in the Windows Start Menu.
Once everything is installed correctly, open D+. Two windows should open.
One looks like a command prompt, the other should look something like Figure \ref{fig:openingWindow}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.635\textwidth]{openingWindow}
	\caption[The main window]{The main window contains multiple ``dockable'' tabs that can be placed based on personal preference. Each tab is addressed in this manual.}
	\label{fig:openingWindow}
\end{figure}

We shall address each of the panes here covering the various elements. Their collective use will be covered in chapter \ref{chp:examples}.

\section{Main window} \label{sec:mainWindow}


\begin{wrapfigure}[3]{r}{0.3\textwidth}
	\vspace{-13pt}
	\centering
%	\fbox{
	\includegraphics[width=0.95\linewidth]{fileMenu}
%	}
\end{wrapfigure}

There are a few functions that can be accessed from the main window.
Under the \path{File} menu, \path{Open Signal...} and \path{Close Signal} load and close an experimental signal against which the model will be plotted, compared with, and fit to.
\path{Save All Parameters...} saves all the parameters from the various panes (such as the \path{Domain View}, \path{Parameter Editor}, etc., but not from the \path{Script Editor}) to a file.
\path{Import All Parameters...} then loads all the parameters from that file and updates the graphic user interface (GUI).
The option \path{Save 3D view to file...} is not implemented.
\path{Export 1D Graph...} saves the calculated curve that is displayed in the \path{2D Graph} pane.
\path{Export Amplitude File...} and \path{Export PDB Representation} will be addressed in chapter \ref{chp:examples}.
\path{Quit} exits D+ (surprise!).


\begin{wrapfigure}[8]{r}{0.27\textwidth}
	\vspace{-15pt}
	\centering
%	\fbox{
	\includegraphics[width=0.95\linewidth]{viewMenu}
%	}
%  \caption{Basic layout}
\end{wrapfigure}
Figure \ref{fig:openingWindow} shows a possible layout of the main window.
Change the layout based on personal preference.
If a particular window or pane is closed, it can be reopened by going to the \path{View} menu and selecting the closed pane.
This also brings panes that are tabs behind other tabs to the forefront of the other tabs.
If you are looking for a pane that seems to have disappeared, this is a good place to help find it.

\begin{wrapfigure}[4]{r}{0.25\textwidth}
	\vspace{-13pt}
	\centering
%	\fbox{
	\includegraphics[width=0.95\linewidth]{editMenu}
%	}
\end{wrapfigure}
To save a particular layout, go to the \path{Edit} menu and select \path{Save Layout...}.
A layout can be loaded by selecting \path{Load Layout...}.
When closing D+, a file named \path{Latest.dlayout} will be created in the directory (or in \path{%appdata%}) so that the layout is preserved from session to session.
The \path{Default Layout} is just a ``common ground'' layout and not a particularly comfortable one.
Create one that feels right for you and save your own layout. 

\begin{wrapfigure}[3]{r}{0.35\textwidth}
	\vspace{-13pt}
	\centering
%	\fbox{
	\includegraphics[width=0.95\linewidth]{settingsMenu}
%	}
\end{wrapfigure}
Other elements of the main window include the \path{Settings} menu and a status strip on the bottom of the  window.
%The \path{Settings} menu has two items that affect how the GUI is updated when fitting.
%The ``Live Generation'' option should never be checked.
%There is no good reason for its existence and should have been burninated long ago.
The ``Configure Server'' option allows you to specify the server address and activation code.
The status strip at the bottom of the main opening window (Figure \ref{fig:openingWindow}) should just have the word ``Idle'' on the left, as there is nothing happening when opening D+.
Later when using D+, there may be different statuses as well as a progress bar.

\begin{wrapfigure}[2]{r}{0.25\textwidth}
	\vspace{-7pt}
	\centering
%	\fbox{
	\includegraphics[width=0.95\linewidth]{mainWindowButtons}
%	}
\end{wrapfigure}

In addition to the menus, there is a row of buttons near the top of the main window. The left four (\path{Open Signal...}, \path{Close Signal}, \path{Open Parameter File...} and \path{Save Parameters}) correspond to menu items in the \path{File} menu. The \path{Undo} and \path{Redo} buttons are unimplemented.
% The right-most button (\path{Toggle console}) will open and close the window that looks like a command prompt. Note that closing it will result in its contents being cleared.
% This window is used to provide supplementary information when using D+ and can help in identifying problems when they arise.

\section{Domain View} \label{sec:domainView}

The \path{Domain View} window contains the list of models that will be used to calculate a solution Small Angle X-Ray Scattering (SAXS) curve. We shall go over each element here.

%\begin{wrapfigure}{r}{0.35\textwidth}
%	\vspace{-13pt}
%	\centering
%%	\fbox{
%    \begin{tabular}{@{}c@{}}
%    \includegraphics[width=0.95\linewidth]{DomainViewBlank} \\% Dummy image replacement
%    \includegraphics[width=0.95\linewidth]{DomainViewModelMenu} \\% Dummy image replacement
%  %  \textcolor{red}{\rule{3cm}{3cm}} \\% Dummy image replacement
%    \end{tabular}
%%	}
%\end{wrapfigure}

\begin{wrapfigure}[6]{r}{0.35\textwidth}
	\vspace{-30pt}
	\hspace{-15pt}
	\centering
%	\fbox{
    \includegraphics[width=0.95\linewidth]{DomainViewBlank}
%	}
\end{wrapfigure}

First, you will note that on the top, there is a tab called ``Population 1''. Multiple populations can be used. Different populations are assume to have no correlations between them and therefore the scattering intensities (rather than amplitudes) are summed. Within each population there are correlations between the positions and orientations of subunits and therefore the scattering amplitudes of subunits are added. For explanations regarding the difference, see the published literature \textcite{Ben-NunX+,als2011elements, Kittel2005}. Each population has an ``Average Population Size'' (default is 1). All the population sizes are given their respective weights, $w_i$, and the sum is divided by the total. In other words, the fraction of each population is given by $\nicefrac{w_i}{\sum w_i}$. To add a population, press the ``+'' button to the right of the tab(s). Populations can be renamed for convenience by right clicking on the population tab and choosing \path{Rename} or \path{F2} (the \path{F2} option requires using the mouse before being able to press \path{F2}). Populations can be deleted by center clicking on the relevant tab or choosing \path{Close Population} from the context menu. The Mutable checkbox refers to the Average Population Size.

\begin{wrapfigure}[11]{r}{0.35\textwidth}
	\vspace{-10pt}
	\centering
%	\fbox{
    \includegraphics[width=0.95\linewidth]{DomainViewModelMenu}
%	}
\end{wrapfigure}

To add models to the population, select a model from the drop down menu on the lower left and press the ``Add'' button below it. The models are divided into three categories: geometric models; symmetries; and other. Other includes loading a PDB file, and loading a pre-calculated amplitude grid (AMP) from a file. The grid is essentially a lookup table of amplitudes in the 3D reciprocal space ($\vec{q}$) used by D+ to calculate the scattering amplitude (and then intensity) of larger and more complex structures. Precise descriptions of the geometric models can be found in the paper ``Solution X-ray scattering form factors of supramolecular self-assembled structures'' \textcite{szekely2010solution}. The ``symmetries'' will be addressed in full in chapter \ref{chp:Symmetries}, but for now, just note that they must ``wrap'' other models. If one or more models are selected in the Entities box (from here on, the entity tree) and a symmetry is selected in the drop down menu, the ``Group Selected'' button becomes active. Pressing it will ``wrap'' the selected model(s) in a new symmetry. Models in the entity tree can be moved around by dragging and dropping. To remove a model, select it and press the ``Remove'' button or the delete key.

When the drop-down box is on PDB File or Amplitude Grid, a Center PDB checkbox appears.
When loading a PDB with the box checked, the atomic coordinates are translated such that the center of mass coincides with the origin.
For an Amplitude File, the checkbox does not change anything except indicating that the box was checked in the headers of the resulting outputs.
Try it.
Download a PDB file from the \href{http://www.rcsb.org/pdb/home/home.do}{Protein Data Bank}.
Load the file with and without the checkbox checked.


\section{Symmetry Editor} \label{sec:symmetryEditor}

\begin{wrapfigure}{r}{0.35\textwidth}
	\vspace{-10pt}
	\centering
%	\fbox{
    \includegraphics[width=0.95\linewidth]{symmetryEditorBlank}
%	}
\end{wrapfigure}

When a single item is selected in the entity tree, the Symmetry Editor window is activated. The $x$, $y$, $z$ fields are the items location relative to the origin. The \textit{alpha}, \textit{beta}, \textit{gamma} fields dictate the items orientation.  The edit constraints button opens up a window allowing constraints to be set (only the absolute minimum/maximum are implemented). The ``Use Grid From Here'' checkbox is for ``hybrid'' calculations and will be explained in more detail in section \ref{sec:hybrid}.

\section{Command Window} \label{sec:commandWindow}

The command window provides access to some of the inner workings of D+. It can also be used as a calculator. The language used is \href{http://www.lua.org/}{Lua}. For example, typing \lstinline[language={[5.0]Lua}]|math.pi^2| at the prompt gives \lstinline[language={[5.0]Lua}]|ans = 9.86960440108936|. Everything that can be written in a script can be entered here. See chapter \ref{chp:scripts} for details.

\section{Script Editor} \label{sec:scriptEditor}

This pane is a basic \href{http://www.lua.org/}{Lua} script editor. There are buttons to open, save, cut, paste, etc. The main advantage of this pane is the ability to run, pause and stop scripts. Breakpoints can also be set by using the ``Add/Remove Breakpoint'' button or by clicking next to the line numbers. See chapter \ref{chp:scripts} for details on scripting.



\section{Parameter Editor} \label{sec:parameterEditor}

\begin{figure}[h!] %{r}{0.5\textwidth}
%	\vspace{-10pt}
	\centering
%	\fbox{
    \includegraphics[width=0.55\linewidth]{parameterEditorBasicCylinder}
%	}
%	\vspace{-10pt}
	\caption{Parameter editor pane. Upper panel contains the model parameters of each layer (e.g., size or electron density (E.D.)). The lower pane contains extra global parameters that apply to all the layers. }
	\label{fig:ParameterEditor}
\end{figure}

The Parameter Editor is where the parameters of each node in the entity tree can be edited. When one entity is selected, its parameters appear here (Figure \ref{fig:ParameterEditor}). The upper panel contains a table of the model parameters. These parameters are often organized by layers, with the number of layers being variable. The bottom pane contains the ``Extra Parameters'' which is a fixed number of parameters characteristic of each model.

\section{3D Graph} \label{sec:3DGraph}

\begin{wrapfigure}{r}{0.5\textwidth}
	\vspace{-10pt}
	\centering
%	\fbox{
    \includegraphics[width=0.95\linewidth]{3DGraph}
%	}
	\vspace{-10pt}
\end{wrapfigure}

The 3D Graph pane shows the real-space (or ``real-world'') picture whose SAXS signal will be calculated. It can be rotated by holding down the right mouse button and dragging. Zoom is controlled by the scroll wheel. Items can be selected using the left mouse button. Dragging with the center mouse button changes the point upon which the camera focuses. Double clicking the center button returns the viewport to a semi-default setup. The camera can also be controlled from the Controls pane (sec. \ref{sec:controls}).

\section{2D Graph} \label{sec:2DGraph}

This window displays the calculated 2D graph of $I\left(q\right)$, which is the scattering  intensity, $I$, as a function of the magnitude of the scattering vector, $q$, of the model described in ``Domain View''. The model is displayed in blue, while a loaded signal is red. A loaded signal is usually the signal that we are trying to fit.


\section{Controls} 
\label{sec:controls}



The Controls pane contains two main parts. The upper part, labeled 3D View, controls the camera for the 3D Graph (sec. \ref{sec:3DGraph}). There are the roll, pitch and yaw parameters as well as zoom. Two checkboxes control the display of the red, blue and green axes in the 3D Graph.

\begin{wrapfigure}[4]{r}{0.35\textwidth}
	\vspace{-10pt}
	\centering
%	\fbox{
    \includegraphics[width=0.95\linewidth]{controls}
%	}
	\vspace{-10pt}
\end{wrapfigure}

The second part controls the generation and fitting of models. The domain scale (and its mutability checkbox) is a scale that the entire model is multiplied by. The ``Generate'' button starts the calculation of the current model. ``Fit'' will attempt to change the mutable parameters such that the model will better fit the loaded signal. The ``Stop'' button signals the calculation to abort. Note that stopping may not be immediate.

Single Geometry Mode has been deprecated and is unavailable.


\section{Preferences} \label{sec:preferences}

\begin{wrapfigure}{r}{0.35\textwidth}
	\vspace{-10pt}
	\centering
%	\fbox{
    \includegraphics[width=0.95\linewidth]{preferences}
%	}
	\vspace{-10pt}
\end{wrapfigure}

The Preferences pane controls several parameters regarding the calculation of the modeled SAXS curve. The  ``Integration Method,'' ``Integration Iterations'' and ``Convergence'' fields control how D+ calculates the orientation average of the model (more on this in chapter \ref{chp:integrationMethods}). The ``Use Grid'' checkbox determines whether or not lookup tables are allowed to be used in the calculations. Unchecking it may cause certain selection combinations not to work (\textit{e.g.} VEGAS). ``$q$ Max'' is the greatest $q$ value for which the scattering intensity curve of the model will be calculated. ``Generated Points'' determines the number of points on the $q$-axis that will be generated. Note that the above two parameters will be hidden when a signal is loaded and they will be determined based on the loaded signal. The ``Grid Size'' is a measure of how dense the reciprocal-space grid (or lookup table) will be. There will be ``Grid Size''$/2$ points between 0 and ``$q$ Max'' not including the origin. The reason for this factor is that reciprocal space grids have both negative and positive values of $\vec{q}$.

At the bottom of the pane there are two controls that affect the 3D Graph pane. If the level of detail is too high, rendering the scene (i.e, generating the image of the real-space model) will take a long time.

\section{Fitting Preferences} \label{sec:fittingPreferences}

\begin{wrapfigure}{r}{0.35\textwidth}
	\vspace{-20pt}
	\centering
%	\fbox{
    \includegraphics[width=0.95\linewidth]{fittingPreferences}
%	}
	\vspace{-10pt}
\end{wrapfigure}

D+ uses \href{http://ceres-solver.org/}{Ceres-Solver} to try to fit mutable parameters to the data signal. This pane allows access to the various methods Ceres uses.
% Note that not all combinations are valid (\textit{e.g} (L)BFGS cannot be used with Armijo). 
The (black) command prompt window will display messages when problems of this sort occur. These parameters dictate how the fitting will be performed when pressing the Fit button in the \hyperref[sec:controls]{Controls} pane. To better know how to use the various combination of options and parameters see, for example,  \href{http://ceres-solver.org/}{Ceres-Solver}.


\end{document}


