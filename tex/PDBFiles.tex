
\documentclass[../D+Manual.tex]{subfiles}
\begin{document}
\chapter{PDB (Atomic) Objects}

\begin{quote}
	My religion consists of a humble admiration of the illimitable superior spirit who reveals himself in the slight details we are able to perceive with our frail and feeble mind.\\
	\hspace*{\fill} \textit{Albert Einstein}
\end{quote}

Using an atomic description of a molecule gives a much higher resolution picture than using geometric models. For example, consider the tubulin hetero-dimer. It is comprised of an $\alpha$ and a $\beta$ dimer, both roughly spheres. However, the SAXS pattern of a pair of spheres is significantly different than that of the $\alpha\beta$ dimer. Using atomic representations can lead to much improved models, especially at higher resolutions.


\section{PDB Files}

PDB files can be obtained from multiple sources. When downloaded from the \href{http://www.rcsb.org/pdb/home/home.do}{protein data bank}, the files usually work as-is. PDB files from simulations often disobey the \href{http://deposit.rcsb.org/adit/docs/pdb_atom_format.html#ATOM}{standard} and have various extensions.

A PDB file is a text representation of a collection atoms. From a certain point, atoms are represented like the following example:\\
\begin{lstlisting}[basicstyle=\tiny,breaklines=false,backgroundcolor=\color{gray!15},linewidth=1.07\linewidth]
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
ATOM    145  N   VAL A  25      32.433  16.336  57.540  1.00 11.92      A1   N  
\end{lstlisting}

D+ requires that there be at least 78 characters per \texttt{ATOM} or \texttt{HETATM} (which stands for hetero atom) line, as the atom type is determined by positions 77-78.
D+ reads and stores all the information, but the minimal information needed is shown here between the $\lfloor$ and $\rfloor$ symbols. Note that the spacing is critical.

\begin{lstlisting}[basicstyle=\tiny,escapeinside={(*}{*)},mathescape=true,backgroundcolor=\color{gray!15},linewidth=1.07\linewidth]
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
ATOM    145  N   VAL A  25      32.433  16.336  57.540  1.00 11.92      A1   N1+
(*\hspace{-3pt} $\lfloor$ \hspace{3pt}*)   (* $\rfloor$ *)                       (*\hspace{-3pt} $\lfloor$ \hspace{3pt}*)     (* $\rfloor$ *)(*\hspace{-6.5pt} $\lfloor$ \hspace{3pt}*)     (* $\rfloor$ *)(*\hspace{-6.5pt} $\lfloor$ \hspace{3pt}*)     (* $\rfloor$ *)                     (*\hspace{-2pt} $\lfloor$ \hspace{0pt}*)(* $\rfloor$ *)
\end{lstlisting}

If there is a formal charge on the atom, it must appear in columns 79-80 of the line. \texttt{HETATM} is processed exactly like \texttt{ATOM}. Note that some simulations use various symbols to represent water molecules and these are not processed as such by D+. You will have to change the atom type to \ce{H} and \ce{O} manually.

\section{PDB Objects in D+}

With high resolution data it is often useful to describe a protein structure as a collection of protein subunits.
The subunits can often be resolved using other methods (crystallography, simulations, etc.) and their results are usually available as PDB files.
One of the most important models D+ offers (accessible from the \hyperref[sec:domainView]{Domain View}) is that of a PDB file.
Once loaded, its parameters as shown in the \hyperref[sec:parameterEditor]{Parameter Editor} look like the following image.
\begin{figure}[h]
\centering
\includegraphics[width=0.9\linewidth]{PDBParameterEditor}
%\caption{}
\label{fig:PDBParameterEditor}
\end{figure}
\begin{wrapfigure}{r}{0.2\textwidth}
	\vspace{-20pt}
	\centering
	\includegraphics[width=0.9\linewidth]{PDBSolventMethods}
	\vspace{-30pt}
\end{wrapfigure}
Besides the scale, all the parameters relate to how the solvents scattering amplitude is subtracted from the atomic scattering amplitude.
When considering geometric shapes (cylinders, spheres, etc.), the electron density \textit{contrast} is trivial to obtain, as it is just the one shape minus a similar shape representing the solvent with a different electron density.
In the case of a collection of atoms, treating the solvent is not so trivial.
D+ offers two main methods to subtract the solvents contribution, as well as that from the solvation layer.

\subsection{Gaussian ``Dummy'' Atoms}

\textcite{fraser1978improved} proposed subtracting a Gaussian sphere
\begin{equation*}\rho\left( r\right) \propto e^{-\left( \frac{r}{r_0}\right) ^2}\end{equation*}
from each atom (located at the same coordinate).
Following that logic, we can subtract the Fourier Transform of a normalized Gaussian sphere:
\begin{equation*}
	F\left( q \right) = \rho_{0}\cdot V_j \cdot\exp\left[-\frac{V_j^{2/3}\cdot q^2}{4\pi}\right]
\end{equation*}
where $V_j=\frac{4\pi}{3}r^3_j$
%The contribution of the solvent is obtained by the Fourier transform of the electron density:
%
%\begin{equation*}
%F\left( q \right) = \pi^{\frac{3}{2}}r_0^3 \rho_{0} e^{-\pi^2 r_0^2 q^2}
%\end{equation*}
The scattering amplitude from atom $i$ after subtracting the contribution of the solvent is:
\begin{equation*}
f^0_i\left( q \right) -F\left( q \right) 
\end{equation*}
where $f^0_i\left( q \right)$ is the scattering amplitude of the atom \textit{in vacuo}, $\rho_{0}$ is the mean solvent electron density, and $r_0$ is an effective atomic radius (dictated by the type of atom).

This method is coined ``Dummy Atoms'' in the drop down menu above.
All the other options pertain to the following method.

\subsection{Voxel Based Solvent}

An alternative method of subtracting the solvent is to determine the shape that is the union of all the atoms and subtract that (at the electron density of the solvent).
This was proposed by \textcite{Pavlov}.
This methods makes a few assumptions and has some shortcomings.

The solvent excluded volume is calculated in several stages.
First of all, space is discretized into voxels of a specific size (the \texttt{Solvent Voxel Size} parameter in $\unit{nm}$).
Each voxel is marked as occupied if at least one atom's coordinate is within that atom's radius (the atomic radius type can be chosen in the \texttt{Solvent method} parameter).
This represents the excluded volume of the atomic structure.
Voxels that are within \texttt{Solvent Radius} (in \unit{nm}) of an atom are marked as occupied by a solvation layer, coined \texttt{Outer Solvent}.
Voxels that are completely enveloped by the union of the two shapes are considered holes.
If the \texttt{Fill Holes} checkbox is checked, these voxels will be added to the shape that represents the volume of the atoms (as excluded volume).
The excluded volume shape with an electron density of \texttt{Solvent ED} is subtracted from the atomic contributions.
The solvation layer represents the excess electron density contrast owing to a higher or lower density of the solvent near the interface.

As of the current version at the time of writing, this option is not fully optimized and can take a significant amount of time.


\subsection{Anomalous Scattering} \label{sec:anomalousScattering}
D+ can take anomalous scattering of atoms into account when computing scattering amplitudes of atomic model. Two additional correction terms should be added to get the atomic form factor energy dependence:
\begin{equation*}
	f(q,\lambda) = f^0(q) + f'(\lambda) + i f''(\lambda).
\end{equation*}
To enable this option for a given PDB file, choose \texttt{PDB File...} from the dropdown menu in the \hyperref[sec:domainView]{Domain View} window.
Check the \texttt{Anomalous} checkbox \textit{before} pressing the \texttt{Add} button.
An additional open file dialog will pop up after selecting the PDB file.
Select the text file containing the relevant $f'$ and $f''$ values.
The file should be formated using any combination of the lines below.

\lstset{style=anomalousFile,}

\begin{lstlisting}
# Energy    2345.82km
# <f'>  <f''>   <atom identifier>
1.2     2.3     Fe2+
#<f'>   <f''>   <atom indices>
1.3     3.4     125     256
#<f'>   <f''>   <name of a group that's defined below>
-3e4    +2.01   $(someRandomGroupName)
#<name of a group>      <list of atom indices>
$(someRandomGroupName)  252 472 7344 313
\end{lstlisting}

Lines that start with '\#' are considered comments and are ignored.


\printbibliography[heading=subbibliography]
\end{document}