
\documentclass[../D+Manual.tex]{subfiles}
\begin{document}
	
\chapter{Python API} \label{chp:Python}

\begin{quote}
	You can start any 'Monty Python' routine and people finish it for you. Everyone knows it like shorthand. \\
	\hspace*{\fill} \textit{Robin Williams}
\end{quote}


\section{The DPlus Python API}\label{the-dplus-python-api}

The D+ Python API allows using the D+ backend from Python, instead of
the ordinary D+ application.

The Python API works on both Windows and Linux.

\subsection{Installation}\label{installation}

Installing the Python API is done using PIP:

\begin{lstlisting}[language=bash,basicstyle=\small, breaklines= true, breakatwhitespace= true]
pip install --extra-index-url https://testpypi.python.org/pypi dplus-api
\end{lstlisting}
In the future, the \path{--extra-index-url} should not be needed.

The API was tested with Python 3.5 and newer. It \emph{may} work with
older versions of Python, although Python 2 is probably not supported.

\subsection{Quick example}\label{quick-example}

Here is an example that generates a signal from a state file created in
D+:

\begin{lstlisting}[style=pythonstyle]
from dplus.CalculationRunner import LocalRunner
from dplus.CalculationInput import GenerateInput

api = LocalRunner()
input = GenerateInput.load_from_state(<name of state file>)
result = api.generate(input)

print (result.graph)
\end{lstlisting}

\texttt{LocalRunner} is a wrapper that runs a local installation of the
D+ backend.

\texttt{input} is the Python representation of the state file, ready for
generation.

\texttt{api.generate(input)} generates the signal of the state file.

Finally, \texttt{result.graph} is an OrderedDict with the generated
signal graph (\texttt{graph{[}x{]}} is the y value at that coordinate)

\subsection{Accessing models and
properties}\label{accessing-models-and-properties}

After reading a state file it is possible to access and modify all its
properties.

You can access a model like so:

\begin{lstlisting}[style=pythonstyle]
model = input.get_model(<model-name-or-ptr>)
\end{lstlisting}

Now you can access all the model properties, for example:

\begin{itemize}
\tightlist
\item
  \texttt{model.model\_ptr} is the model pointer.
\item
  \texttt{model.name} is the model's name
\item
  \texttt{model.layer\_params{[}0{]}{[}'Radius'{]}}
  is the Radius parameter of layer 0.
\end{itemize}

You can also access parameter values:

\begin{lstlisting}[style=pythonstyle,literate={'}{{'}}1]
param = model.layer_params[1]['Radius'] print(param.value)
param.mutable = True
\end{lstlisting}
This code prints the Radius parameter value and marks the parameter as
Mutable.

\subsection{Fitting a signal}\label{fitting-a-signal}

It is also possible to fit the parameters to a signal. One way to do
that is to prepare everything in D+, set the mutable parameters, load a
signal file and:

\begin{lstlisting}[style=pythonstyle]
 api = LocalRunner()
 input = FitInput.load_from_state(<name of state file>)
 result = api.fit(input)
 input.combine_results(result)
\end{lstlisting}

This code will load a state file that already contains a reference to a
signal file, run the fit process and combine the results back into the
state.

It is also possible to modify the state in Python:

\begin{lstlisting}[style=pythonstyle,literate={'}{{'}}1]
 input = GenerateInput.load_from_state(...)
 model = input.get_model(...)
 param = model.extra_params['Solvent']
 param.value = 40
 param.mutable = True
 generate_result = api.generate(input)
 
 fit_input = FitInput(input.state, generate_result.graph)
 api.fit(fit_input)
\end{lstlisting}

\subsection{The various state classes}\label{the-various-state-classes}

\subsubsection{\texorpdfstring{\texttt{State}}{State}}\label{state}

The \texttt{State} class contains the following properties:

\begin{itemize}
\tightlist
\item
  \texttt{DomainPreferences}
\item
  \texttt{FittingPreferences}
\end{itemize}

These are classes containing the same kind of information as the D+ dialogs.
It is possible to change their values, for example:
\begin{lstlisting}[style=pythonstyle]
s=State()
s.DomainPreferences.orientation_method="Monte Carlo (Mersenne Twister)"
\end{lstlisting}
Only very specific values are allowed. Forbidden values will raise a ValueError.
Refer to the D+ documentation for further information.

\begin{itemize}
	\tightlist
	\item
	\texttt{Domain}
\end{itemize}

This is the top domain model.

\subsubsection{\texorpdfstring{\texttt{Model}}{Model}}\label{model}

The \texttt{Model} class contains the following properties:

\begin{itemize}
	\tightlist
	\item
	\texttt{model\_ptr}
	\item
	\texttt{name}
	\item
	\texttt{Children}
	\item
	\texttt{layer\_params}
	\item
	\texttt{extra\_params}
	\item
	\texttt{location\_params}
\end{itemize}

Not all properties are available for all models. Models without children
do not have the \texttt{Children} property, for example.

\subsubsection{\texorpdfstring{\texttt{Param}}{Param}}\label{param}

The \texttt{Param} class contains the following:

\begin{itemize}
	\tightlist
	\item
	\texttt{value}
	\item
	\texttt{sigma}
	\item
	\texttt{constraints}
	\item
	\texttt{mutable}
\end{itemize}


\subsection{Other Classes}\label{sec:other-classes}

The Dplus CalculationRunner Module has two classes of \lstinline[style=pythonstyle]|CalculationRunner|, \lstinline[style=pythonstyle]|LocalRunner| and \lstinline[style=pythonstyle]|WebRunner|.
The \lstinline[style=pythonstyle]|LocalRunner| can be used to run D+ locally if there is local access to the D+ executables.
It requires the address of the folder in which the D+ executables are stored (if D+ has been installed on Windows using the Windows installer the API can find this folder automatically), and optionally the address of the folder in which the D+ files should be saved (they will otherwise be saved in a temporary folder).
The \lstinline[style=pythonstyle]|WebRunner| can be used to run D+ remotely by way of the web server.
It requires the server address and the user's authentication token. 
For the present we will be giving examples using the \lstinline[style=pythonstyle]|LocalRunner| only.

The Dplus State Module contains the \lstinline[style=pythonstyle]|State| class.
States contain all the information necessary for the D+ executable to run a calculation.
Each state contains within it the classes \lstinline[style=pythonstyle]|DomainPreferences| and \lstinline[style=pythonstyle]|FittingPreferences|, as well as a hierarchical tree of instances of \lstinline[style=pythonstyle]|Model| classes called ``Domain''.
The various types of models are stored in the Dplus Module \lstinline[style=pythonstyle]|DataModels|.
It is possible to populate an instance of state through the \lstinline[style=pythonstyle]|load_from_json| function (explained further below) or to build one manually.

The DPlus \lstinline[style=pythonstyle]|CalculationInput| module has two classes of input, \lstinline[style=pythonstyle]|GenerateInput| and \lstinline[style=pythonstyle]|FitInput|, meant for Generate and Fit calculations respectively.
Each can be populated via their \lstinline[style=pythonstyle]|load_from_state| functions.

The Dplus \lstinline[style=pythonstyle]|CalculationResult| module contains the very simple container class CalculationResult.
It is possible to get the headers from the result, the graph from a generate result, or the fitted parameter tree from a fit result.


\subsection{Further Usage Examples} \label{sec:Further-Usage-Examples}

From the D+ GUI it is possible to create a state file by selecting File>Export All Parameters.
Alternately one can create a State by hand, by adding populations, models, fittingpreferences, etc.
In addition, there is an option for generating a pdb, \lstinline[style=pythonstyle]|load_from_pdb|. It requires the address of the pdb file, and the value of $q_\text{max}$ (the largest q value). It automatically populates the rest of the state with reasonable default values.


\subsubsection{Example One}

\begin{lstlisting}[style=pythonstyle,literate={'}{{'}}1]
from dplus.CalculationInput import FitInput
from dplus.CalculationRunner import LocalRunner

exe_directory = r"..\x64\release"
sess_directory = r"session"
runner= LocalRunner(exe_directory, sess_directory)


input=FitInput.load_from_state('spherefit.state')
result=runner.fit(input)
print(result.graph)
\end{lstlisting}


\subsubsection{Example Two}


\begin{lstlisting}[style=pythonstyle]
from dplus.CalculationInput import GenerateInput
from dplus.CalculationRunner import LocalRunner
from dplus.DataModels import ModelFactory, Population
from dplus.State import State
from models import UniformHollowCylinder

sess_directory = r"session"
runner= LocalRunner(session_directory=sess_directory)

uhc=UniformHollowCylinder()
s=State()
s.Domain.populations[0].add_model(uhc)

caldata = GenerateInput(s)
result=runner.generate(caldata)
print(result.graph)
\end{lstlisting}


\subsubsection{Example Three}


\begin{lstlisting}[style=pythonstyle,literate={'}{{'}}1]
runner=LocalRunner()

caldata=GenerateInput.load_from_PDB('1JFF.pdb', 5)
result=API.generate(caldata)
print(result.graph)
\end{lstlisting}


\subsubsection{Example Four}


\begin{lstlisting}[style=pythonstyle, literate={'}{{'}}1]
API = test_local()
input = GenerateInput.load_from_state('../example files/uhc.state')
cylinder = input.get_model("Cylinder")

print("Original radius is ", cylinder.layer_params[1]['Radius'].value)
result = API.generate(input)

fit_input = FitInput(input.state, result.graph)
cylinder = fit_input.get_model("Cylinder")
cylinder.layer_params[1]['Radius'].value = 2
cylinder.layer_params[1]['Radius'].mutable = True

fit_result = API.fit(fit_input)
print(fit_result.parameter_tree)
fit_input.combine_results(fit_result)
print("Result radius is ", cylinder.layer_params[1]['Radius'].value)
\end{lstlisting}


\subsection{Python Fitting} \label{sec:python-fitting}

It is possible to fit a curve using the results from Generate and numpy's built in minimzation/curve fitting functions. This is a new functionality that is sill very much under development. An example follows:

\begin{lstlisting}[style=pythonstyle, literate={'}{{'}}1]
import os
import json
from dplus.CalculationInput import GenerateInput, FitInput
from dplus.CalculationData import CalculationData
from dplus.API import LocalCalculationAPI
from dplus.CalculationResult import CalculationResult
from dplus.Fit import Fitter
from dplus.DataModels import State

statePath = r"/home/avi1604/Downloads/FitTest/state.state"
state = State()
state.load_from_json(json.load(statePath))
filename = state.DomainPreferences['SignalFile']
x, y = FitInput._load_x_and_y_from_file(filename)
fitInput = FitInput(state, x=x, y=y)

exe_file = r"/home/avi1604/Downloads/bin"
api = LocalCalculationAPI(exe_file)

fitter = Fitter(api)
result = fitter.run(fitInput)
\end{lstlisting}

\end{document}